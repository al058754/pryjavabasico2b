

  package llamandometodos;

import javax.swing.*;
import java.util.*;
import java.util.Scanner;

/**
 *
 * @author Negriz07
 */
public class Llamandometodos {

    static void presentacion() {
        JOptionPane.showMessageDialog(null, "Hola comunidad de la UACC FI ISC\n"
                + " Daniel Obed Chi Navarrete 58754\n"
                + " Tema:  EL CORONAVIRUS-COVID-19");
    }

    static void informacion() {
        JOptionPane.showMessageDialog(null, "El COVID-19 es un brote de enfermedad respiratoria causada por un nuevo coronavirus que se identificó por primera vez en Wuhan.\n En los casos confirmados de la enfermedad del coronavirus 2019 (COVID-19)");
    }

    static void sintomas() {
        JOptionPane.showMessageDialog(null, "Los síntomas pueden incluir:\n"
                + " 1.-   Fiebre\n"
                + " 2.-    Tos\n"
                + " 3.-   Dificultad para respirar");

    }

    static void cuidados() {
        JOptionPane.showMessageDialog(null, " 1.- Lavse las manos constantemente\n " + " 2.- Si no hay agua y jabón fácilmente disponibles, "
                + " 3.- Use un desinfectante de manos que contenga al menos un 60 % de alcohol.\n " + " 4.- Evite tocarse los ojos, la nariz y la boca con las manos sin lavar.");
    }

    static void ciudadesconcasos() {
        JOptionPane.showMessageDialog(null, "1.- Campeche: 2 casos\n " + "2.- Yucatan: 29 casos\n " + "3.-Quintana ROO: 128 casos");
    }

    public static void main(String[] args) {

        int opc = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número de información que desea saber\n" + "1: Presentacion\n"
        +"2: Informacion\n"+ "3: Sintomas\n" + "4: Cuidados\n" + "5: Cuidades con casos de COVID-19"));
        if (opc == 1) {
            presentacion();
        }

        if (opc == 2) {
            informacion();
        }
        if (opc == 3) {
            sintomas();
        }

        if (opc == 4) {
            cuidados();
        }
        if (opc == 5) {
            ciudadesconcasos();
        }
    }
}
                   
                  